package com.example.my.badestellenfinder;

import android.content.Context;

import com.example.my.asyncTasks.Filter;
import com.example.my.helper.Badestelle;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class FilterTest {

    Badestelle bs;
    ArrayList<Badestelle> bsList;
    Context context;
    private HashMap<String, Integer>  filterSetting;

    @Before
    public void setUp() {
        bs  = new Badestelle(1, "test", "black", "testlink", 12.3, 12.3, 1, 130, 260, 30, 15, 24);
        bsList = new ArrayList<>();
        bsList.add(bs);
    }


    @Test
    public void bsDoesFitWaterFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("waterquality", 1);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(true, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesNotFitWaterFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("waterquality", 0);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(false, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesFitSichtiefeFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("sichttiefe", 100);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(true, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesNotSichtiefeFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("sichttiefe", 150);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(false, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
     public void bsDoesFitCbValueFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("cbValue", 300);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(true, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesNotCbValueFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("cbValue", 200);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(false, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
     public void bsDoesFitEcValueFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("ecValue", 40);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(true, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesNotEcValueFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("ecValue", 20);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(false, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesFitIeValueFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("ieValue", 30);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(true, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesNotIeValueFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("ieValue", 10);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(false, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesFitTempValueFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("tempValue", 23);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(true, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }

    @Test
    public void bsDoesNotTempValueFilter() {
        filterSetting = new HashMap<String, Integer>();
        filterSetting.put("tempValue", 26);
        Filter newFilter = new Filter(context, bsList, filterSetting);
        assertEquals(false, newFilter.doesBadestelleFitFilter(bs, filterSetting));
    }
}
