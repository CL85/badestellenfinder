package com.example.my.badestellenfinder;

import android.content.Context;

import com.example.my.asyncTasks.RadiusSearch;
import com.example.my.helper.Badestelle;
import com.example.my.helper.MyLocation;

import org.junit.Test;

import java.util.ArrayList;


import static org.junit.Assert.assertEquals;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */
public class RadiusSearchTest {


    @Test
    public void testSearchFunction() {
        Context context = null;

        Badestelle bs1 = new Badestelle();
        Badestelle bs2 = new Badestelle();
        Badestelle bs3 = new Badestelle();

        bs1.setLat(52.524248); //not in Radius
        bs1.setLon(13.222632);
        bs2.setLat(52.524248); //not in Radius
        bs2.setLon(13.222632);
        bs3.setLat(52.517888); //in Radius
        bs3.setLon(13.401198);

        ArrayList<Badestelle> listToSearch = new ArrayList<>();
        listToSearch.add(bs1);
        listToSearch.add(bs2);
        listToSearch.add(bs3);

        MyLocation userLocation = new MyLocation(52.517092, 13.403994);

        double searchRadius = 0.5;

        RadiusSearch newSearch = new RadiusSearch(context, listToSearch, userLocation, searchRadius);
        ArrayList<Badestelle> resultList = newSearch.searchFor();
        assertEquals(1, resultList.size());
    }


}
