package com.example.my.badestellenfinder;


import com.example.my.parser.JSONParser;

import org.json.JSONObject;
import org.junit.Test;


import static org.junit.Assert.assertNotNull;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class ParserTest {

    private JSONParser jsonParser = new JSONParser();

    @Test
    public void doesParserReturnNotNullFromValidUrl() {
        JSONObject test = jsonParser.getJSONfromUrl("http://www.berlin.de/lageso/gesundheit/gesundheitsschutz/badegewaesser/liste-der-badestellen/index.php/index/all.gjson?q=");
        assertNotNull(test);
    }

}



