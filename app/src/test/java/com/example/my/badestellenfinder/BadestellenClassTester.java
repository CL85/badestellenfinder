package com.example.my.badestellenfinder;


import com.example.my.helper.Badestelle;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class BadestellenClassTester {


    @Test
    public void doesContructorReturnBadestellenObj() {
        Badestelle test = new Badestelle();
        assertThat(test, instanceOf(Badestelle.class));
    }

    @Test
    public void idGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getId(), instanceOf(Integer.class));
    }

    @Test
    public void nameGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getName(), instanceOf(String.class));
    }

    @Test
    public void colourGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getColour(), instanceOf(String.class));
    }

    @Test
    public void linkToBadestelleGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getLinkToBadestelle(), instanceOf(String.class));
    }

    @Test
    public void latGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getLat(), instanceOf(Double.class));
    }

    @Test
    public void lonGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getLon(), instanceOf(Double.class));
    }

    @Test
    public void waterQualityGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getWaterQuality(), instanceOf(Integer.class));
    }

    @Test
    public void sichttiefeGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getSichttiefe(), instanceOf(Integer.class));
    }

    @Test
    public void cbValueGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getCbValue(), instanceOf(Integer.class));
    }

    @Test
    public void ecValueGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getEcValue(), instanceOf(Integer.class));
    }

    @Test
    public void ieValueGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getIeValue(), instanceOf(Integer.class));
    }

    @Test
    public void temperatureGetter() {
        Badestelle test = new Badestelle();
        assertThat(test.getTemperature(), instanceOf(Double.class));
    }

}
