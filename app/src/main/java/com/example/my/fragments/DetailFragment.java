package com.example.my.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.my.badestellenfinder.R;

import com.example.my.helper.Badestelle;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class DetailFragment extends Fragment implements View.OnClickListener {

    private Badestelle badestelle;

    public DetailFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.detail_fragment_layout, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //get Marker info
        badestelle = (Badestelle) getArguments().getSerializable("badestelle");

        Button closeBtn = (Button) getActivity().findViewById(R.id.closeDetailFragmentBtn);
        closeBtn.setOnClickListener(this);

        TextView nameValueTv = (TextView) getActivity().findViewById(R.id.nameValueTv);
        TextView waterqualityValueTv = (TextView) getActivity().findViewById(R.id.waterqualityValueTv);
        TextView cbValueTv = (TextView) getActivity().findViewById(R.id.cbValueTv);
        TextView ecValueTv = (TextView) getActivity().findViewById(R.id.ecValueTv);
        TextView ieValueTv = (TextView) getActivity().findViewById(R.id.ieValueTv);
        TextView tempValueTv = (TextView) getActivity().findViewById(R.id.tempValueTv);

        if (badestelle != null) {
            nameValueTv.setText(String.valueOf(badestelle.getName()));
            waterqualityValueTv.setText(String.valueOf(badestelle.getWaterQuality()));
            cbValueTv.setText(String.valueOf(badestelle.getCbValue()));
            ecValueTv.setText(String.valueOf(badestelle.getEcValue()));
            ieValueTv.setText(String.valueOf(badestelle.getIeValue()));
            tempValueTv.setText(String.valueOf(badestelle.getTemperature()));
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.closeDetailFragmentBtn:
                FragmentManager fragmentManager = getFragmentManager();
                android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.remove(this);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                fragmentTransaction.commit();
                break;
            default:
                break;
        }

    }
}
