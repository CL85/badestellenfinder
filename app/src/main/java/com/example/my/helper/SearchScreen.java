package com.example.my.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.example.my.badestellenfinder.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import com.example.my.asyncTasks.JSONParse;
import com.example.my.asyncTasks.RadiusSearch;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class SearchScreen implements LocationListener {

    private Context context = null;
    private String pathDir = "";
    private ArrayList<Badestelle> badestellen = null;
    private LocationManager locationManager = null;


    public SearchScreen(Context context) {
        this.context = context;

        this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        this.pathDir = context.getFilesDir().toString() + File.separator + context.getResources().getString(R.string.dataFilename);
        //File file = new File(pathDir);
        //boolean deleted = file.delete();

        this.badestellen = loadDataFromInternalStorage();
        if (badestellen != null) {
            //Check for updates
            if (isNetworkAvailable()) {
                if (checkForUpdates()) {
                    showUpdateDialog();
                }
            }
        } else {
            showNoDataDialog();

        }
    }

    public ArrayList<Badestelle> getBadestellen() {
        return badestellen;
    }


    public ArrayList<Badestelle> doSearch(MyLocation userLocation, double searchRadius) {

        RadiusSearch newSearch = new RadiusSearch(context, badestellen, userLocation, searchRadius);
        try {
            ArrayList<Badestelle> resultList = newSearch.execute().get();
            if (resultList != null && resultList.size() > 0) {
                return resultList;
            } else {
                showNoResultsDialog();
                return null;
            }

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }


    //private methods

    private void updateData() {
        JSONParse jParser = new JSONParse(context, context.getResources().getString(R.string.base_url));
        try {
            JSONObject completeData = jParser.execute().get();
            try {
                ArrayList<Badestelle> badestellenList = Badestelle.badestellenFromJSON(completeData.getJSONArray(context.getResources().getString(R.string.jsonArrayName)));
                saveDataToInternalStorage(badestellenList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }


    private boolean saveDataToInternalStorage(ArrayList<Badestelle> badestellenList) {
        boolean saveSuccess = false;
        if (badestellenList != null) {
            try {
                FileOutputStream fos = context.openFileOutput(context.getResources().getString(R.string.dataFilename), context.MODE_PRIVATE);
                ObjectOutputStream os = new ObjectOutputStream(fos);
                os.writeObject(badestellenList);
                os.close();
                fos.close();
                saveSuccess = true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return saveSuccess;
    }


    private ArrayList<Badestelle> loadDataFromInternalStorage() {
        File file = new File(pathDir);
        ArrayList<Badestelle> badestellen = null;
        if (file.exists()) {
            try {
                FileInputStream fis = context.openFileInput(file.getName());
                ObjectInputStream is = new ObjectInputStream(fis);
                badestellen = (ArrayList<Badestelle>) is.readObject();
                is.close();
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }
        return badestellen;
    }

    private boolean checkForUpdates() {
        boolean isUpdateAvailable = false;
        JSONParse jParser = new JSONParse(context, context.getResources().getString(R.string.base_url));

        try {
            JSONObject completeData = jParser.execute().get();
            //
            try {
                //Getting Data from Open Data Berlin
                int hashValueOpenData = Badestelle.badestellenFromJSON(completeData.getJSONArray(context.getResources().getString(R.string.jsonArrayName))).toString().hashCode();
                int hashValueStoredData = 0;
                ArrayList<Badestelle> storedData = loadDataFromInternalStorage();
                if (storedData != null) {
                    hashValueStoredData = loadDataFromInternalStorage().toString().hashCode();
                }
                if (hashValueOpenData != hashValueStoredData) {
                    isUpdateAvailable = true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return isUpdateAvailable;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    private void showNoDataDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(context.getResources().getString(R.string.noDataDialog));
        alertDialogBuilder.setNegativeButton(context.getResources().getString(R.string.dialogNo), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.setPositiveButton(context.getResources().getString(R.string.dialogYes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                updateData();
                badestellen = loadDataFromInternalStorage();
                //Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(context.getResources().getString(R.string.updateDialog));
        alertDialogBuilder.setNegativeButton(context.getResources().getString(R.string.dialogNo), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialogBuilder.setPositiveButton(context.getResources().getString(R.string.dialogYes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                updateData();
                badestellen = loadDataFromInternalStorage();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showNoResultsDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(context.getResources().getString(R.string.noResultDialog));

        alertDialogBuilder.setPositiveButton(context.getResources().getString(R.string.dialogOk), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
