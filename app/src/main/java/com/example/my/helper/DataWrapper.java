package com.example.my.helper;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class DataWrapper implements Serializable {

    private ArrayList<Badestelle> badestellen;

    public DataWrapper(ArrayList<Badestelle> badestellen) {
        this.badestellen = badestellen;
    }

    public ArrayList<Badestelle> getBadestellen() {
        return badestellen;
    }
}
