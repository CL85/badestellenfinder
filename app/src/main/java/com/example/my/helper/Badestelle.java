package com.example.my.helper;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class Badestelle implements Serializable {

    private int id = 0;
    private String name = "";
    private String colour = "";
    private String linkToBadestelle = "";
    private double lat = 0.0;
    private double lon = 0.0;
    private int waterQuality = 0;
    private int sichttiefe = 0;
    private int cbValue = 0;
    private int ecValue = 0;
    private int ieValue = 0;
    private double temperature = 0.0;

    public Badestelle() {

    }

    public Badestelle(int id, String name, String colour, String linkToBadestelle, double lat, double lon, int waterQuality, int sichttiefe, int cbValue, int ecValue, int ieValue, double temperature) {
        this.id = id;
        this.name = name;
        this.colour = colour;
        this.linkToBadestelle = linkToBadestelle;
        this.lat = lat;
        this.lon = lon;
        this.waterQuality = waterQuality;
        this.sichttiefe = sichttiefe;
        this.cbValue = cbValue;
        this.ecValue = ecValue;
        this.ieValue = ieValue;
        this.temperature = temperature;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColour() {
        return colour;
    }

    public String getLinkToBadestelle() {
        return linkToBadestelle;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public int getWaterQuality() {
        return waterQuality;
    }

    public int getSichttiefe() {
        return sichttiefe;
    }

    public int getCbValue() {
        return cbValue;
    }

    public int getEcValue() {
        return ecValue;
    }

    public int getIeValue() {
        return ieValue;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }



    public static Badestelle badestelleFromJSON(JSONObject jsonObject) {
        Badestelle newBadeStelle = new Badestelle();

        try {
            JSONObject data = jsonObject.getJSONObject("properties").getJSONObject("data");
            newBadeStelle.id = data.getInt("id");
            newBadeStelle.colour = data.getString("farbe");
            newBadeStelle.linkToBadestelle = data.getString("badestellelink");
            newBadeStelle.name = data.getString("badname");
            newBadeStelle.lat = data.getDouble("latitude");
            newBadeStelle.lon = data.getDouble("longitude");
            newBadeStelle.waterQuality = data.getInt("wasserqualitaet");
            String sichttiefe = data.getString("sicht").replace(">", "").replace("<", "");
            String cbValueString = data.getString("cb").replace(">", "").replace("<", "");
            String ecValueString = data.getString("eco").replace(">", "").replace("<", "");
            String ieValueString = data.getString("ente").replace(">", "").replace("<", "");
            newBadeStelle.sichttiefe = Integer.parseInt(sichttiefe);
            newBadeStelle.cbValue = Integer.parseInt(cbValueString);
            newBadeStelle.ecValue = Integer.parseInt(ecValueString);
            newBadeStelle.ieValue = Integer.parseInt(ieValueString);
            newBadeStelle.temperature = Double.parseDouble(data.getString("temp").replace(",", "."));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return newBadeStelle;
    }

    public static ArrayList<Badestelle> badestellenFromJSON(JSONArray jsonArray) {
        ArrayList<Badestelle> badestellenList = new ArrayList<Badestelle>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObjBadestelle = jsonArray.getJSONObject(i);
                badestellenList.add(Badestelle.badestelleFromJSON(jsonObjBadestelle));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return badestellenList;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(name).append("\n");
        sb.append("Wasserqualitaet: ").append(waterQuality).append("\n");
        sb.append("CB Value: ").append(cbValue).append("\n");
        sb.append("EC Value: ").append(ecValue).append("\n");
        sb.append("IE Value: ").append(ieValue).append("\n");
        sb.append("Wassertemperatur: ").append(temperature).append("\n");
        sb.append("Latitude: ").append(lat).append("\n");
        sb.append("Longitude: ").append(lon).append("\n");
        sb.append("Link: ").append(linkToBadestelle).append("\n");

        return sb.toString();
    }
}
