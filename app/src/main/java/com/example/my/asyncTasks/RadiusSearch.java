package com.example.my.asyncTasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


import java.util.ArrayList;

import com.example.my.helper.Badestelle;
import com.example.my.helper.MyLocation;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class RadiusSearch extends AsyncTask<String, String, ArrayList<Badestelle>> {

    private ProgressDialog pDialog;
    private Context context;
    private ArrayList<Badestelle> dataBase = null;
    private MyLocation userLocation = null;
    private double searchRadius = 0.0;
    private final Double EARTH_RADIUS = 6371.0;
    private final int DEGREES = 180;

    public RadiusSearch(Context context, ArrayList<Badestelle> list, MyLocation userLocation, double searchRadius) {
        this.context = context;
        this.dataBase = list;
        this.userLocation = userLocation;
        this.searchRadius = searchRadius;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Searching ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

    }

    @Override
    protected ArrayList<Badestelle> doInBackground(String... params) {

        return searchFor();
    }


    @Override
    protected void onPostExecute(ArrayList<Badestelle> list) {
        pDialog.dismiss();
        // Getting Result list
    }

    public ArrayList<Badestelle> searchFor() {
        ArrayList<Badestelle> resultList = new ArrayList<Badestelle>();

        for (Badestelle bs : dataBase) {
            MyLocation bsLocation = new MyLocation(bs.getLat(), bs.getLon());
            double distance = getDistance(bsLocation, userLocation);
            if (distance <= searchRadius) {
                resultList.add(bs);
            }
        }
        return resultList;
    }


    private double getDistance(MyLocation loc1, MyLocation loc2) {

        double dLat = degToRad(loc2.getLatitude() - loc1.getLatitude());
        double dLon = degToRad(loc2.getLongitude() - loc1.getLongitude());
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(degToRad(loc1.getLatitude()))
                    * Math.cos(degToRad(loc2.getLatitude())) * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS * c; // Distance in km
    }

    private double degToRad(double deg) {
        return deg * (Math.PI / DEGREES);
    }
}
