package com.example.my.asyncTasks;


import android.app.ProgressDialog;
import android.content.Context;

import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.example.my.helper.Badestelle;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class Filter extends AsyncTask<String, String, ArrayList<Badestelle>> {

    private ArrayList<Badestelle> listToFilter = null;
    private ProgressDialog pDialog;
    private Context context;
    private HashMap<String, Integer> filter;

    public Filter(Context context, ArrayList<Badestelle> listToFilter, HashMap<String, Integer> filter) {
        this.listToFilter = listToFilter;
        this.context = context;
        this.filter = filter;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Filtering ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();

    }

    @Override
    protected ArrayList<Badestelle> doInBackground(String... params) {

        return filterList();
    }


    @Override
    protected void onPostExecute(ArrayList<Badestelle> list) {
        pDialog.dismiss();
        // Getting Result list
    }

    public ArrayList<Badestelle> filterList() {
        ArrayList<Badestelle> resultList = new ArrayList<Badestelle>();

        for (Badestelle bs : listToFilter) {
            if (doesBadestelleFitFilter(bs, filter)) {
                resultList.add(bs);
            }
        }
        return resultList;
    }


    public boolean doesBadestelleFitFilter(Badestelle toCheck, HashMap<String, Integer> filter) {
        boolean doesFit = false;
        if (filter.size() == 0) {
            doesFit = true;
        } else {
            int filterKey = filter.size();
            int correctFilter = 0;

            for (Map.Entry<String, Integer> filterEntry : filter.entrySet()) {
                if (doesBadestelleFitSpecificFilter(toCheck, filterEntry.getKey(), filterEntry.getValue())) {
                    correctFilter++;
                }
            }
            if (filterKey == correctFilter) {
                doesFit = true;
            }
        }
        
        return doesFit;
    }


    public boolean doesBadestelleFitSpecificFilter(Badestelle toCheck, String filterKey, int filterValue) {
        boolean doesFitSPecificFilter = false;

        Log.d("filterKey:", filterKey);

        switch (filterKey) {
            case "waterquality":
                if (toCheck.getWaterQuality() <= filterValue) {
                    doesFitSPecificFilter = true;
                }
                break;
            case "sichttiefe":
                if (toCheck.getSichttiefe() >= filterValue) {
                    doesFitSPecificFilter = true;
                }
                break;
            case "cbValue":
                if (toCheck.getCbValue() <= filterValue) {
                    doesFitSPecificFilter = true;
                }
                break;
            case "ecValue":
                if (toCheck.getEcValue() <= filterValue) {
                    doesFitSPecificFilter = true;
                }
                break;
            case "ieValue":
                if (toCheck.getIeValue() <= filterValue) {
                    doesFitSPecificFilter = true;
                }
                break;
            case "tempValue":
                if (toCheck.getTemperature() >= filterValue) {
                    doesFitSPecificFilter = true;
                }
                break;
            default:
                break;
        }
        return doesFitSPecificFilter;
    }

}
