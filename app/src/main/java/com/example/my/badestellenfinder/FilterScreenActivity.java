package com.example.my.badestellenfinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import com.example.my.asyncTasks.Filter;
import com.example.my.helper.Badestelle;
import com.example.my.helper.DataWrapper;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class FilterScreenActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    private TextView waterqualityValueTv;
    private TextView sichttiefeValueTv;
    private TextView cbValueTv;
    private TextView ecValueTv;
    private TextView ieValueTv;
    private TextView tempValueTv;
    private HashMap<String, Integer> filterSettings = null;
    private ArrayList<Badestelle> listToFilter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_screen);
        setTitle("Filter");

        //Setup seekbars
        SeekBar waterquaSb = (SeekBar) findViewById(R.id.waterqualititySb);
        SeekBar sichttiefeSb = (SeekBar) findViewById(R.id.sichttiefeSb);
        SeekBar cbSb = (SeekBar) findViewById(R.id.cbSb);
        SeekBar ecSb = (SeekBar) findViewById(R.id.ecSb);
        SeekBar ieSb = (SeekBar) findViewById(R.id.ieSB);
        SeekBar tempSb = (SeekBar) findViewById(R.id.teperatureSb);

        waterquaSb.setOnSeekBarChangeListener(this);
        sichttiefeSb.setOnSeekBarChangeListener(this);
        cbSb.setOnSeekBarChangeListener(this);
        ecSb.setOnSeekBarChangeListener(this);
        ieSb.setOnSeekBarChangeListener(this);
        tempSb.setOnSeekBarChangeListener(this);

        //Setup TextViews
        waterqualityValueTv = (TextView) findViewById(R.id.waterqualityValueTv);
        sichttiefeValueTv = (TextView) findViewById(R.id.sichttiefeValueTv);
        cbValueTv = (TextView) findViewById(R.id.cbValueTv);
        ecValueTv = (TextView) findViewById(R.id.ecValueTv);
        ieValueTv = (TextView) findViewById(R.id.ieValueTv);
        tempValueTv = (TextView) findViewById(R.id.temperatureValueTv);

        //Setup Buttons
        Button acceptFilterBtn = (Button) findViewById(R.id.filterAcceptBtn);
        acceptFilterBtn.setOnClickListener(this);

        //getting Intent Data
        DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra("badestellen");
        listToFilter = dw.getBadestellen();
        filterSettings = new HashMap<String, Integer>();

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {
            case R.id.waterqualititySb:
                waterqualityValueTv.setText(String.valueOf(progress));
                filterSettings.put("waterquality", progress);
                break;
            case R.id.sichttiefeSb:
                sichttiefeValueTv.setText(String.valueOf(progress));
                filterSettings.put("sichttiefe", progress);
                break;
            case R.id.cbSb:
                cbValueTv.setText(String.valueOf(progress));
                filterSettings.put("cbValue", progress);
                break;
            case R.id.ecSb:
                ecValueTv.setText(String.valueOf(progress));
                filterSettings.put("ecValue", progress);
                break;
            case R.id.ieSB:
                ieValueTv.setText(String.valueOf(progress));
                filterSettings.put("ieValue", progress);
                break;
            case R.id.teperatureSb:
                tempValueTv.setText(String.valueOf(progress));
                filterSettings.put("tempValue", progress);
                break;
            default:
                break;
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filterAcceptBtn:
                //start filter
                ArrayList<Badestelle> filteredList = new ArrayList<Badestelle>();
                try {
                    Filter newFilter = new Filter(this, listToFilter, filterSettings);
                    filteredList = newFilter.execute().get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                //Start finish intetn
                Intent filterIntent = new Intent();
                filterIntent.putExtra("filteredList", new DataWrapper(filteredList));
                setResult(2, filterIntent);
                finish();
                break;
            default:
                break;
        }
    }



}
