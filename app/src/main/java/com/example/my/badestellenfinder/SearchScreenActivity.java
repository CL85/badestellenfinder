package com.example.my.badestellenfinder;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Calendar;

import com.example.my.helper.Badestelle;
import com.example.my.helper.DataWrapper;
import com.example.my.helper.MyLocation;
import com.example.my.helper.SearchScreen;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class SearchScreenActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, LocationListener {

    private String[] spinnerItems = {};
    private Location userLocation = null;
    private LocationManager locationManager = null;
    private SearchScreen searchScreen = null;
    private double searchRadius = 1.0;

    private final int TIME_DIFF = 2 * 60 * 1000;
    private final double[] DISTANCES = {1.0 , 3.0, 5.0, 10.0, 20.0, 50.0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setup
        setContentView(R.layout.activity_search_screen);

        //Setup Search button
        Button startSearch = (Button) findViewById(R.id.searchBtn);
        startSearch.setOnClickListener(this);

        //Setup Spinner
        Spinner rangeSpinner = (Spinner) findViewById(R.id.rangeSpinner);
        spinnerItems = getResources().getStringArray(R.array.rangeSpinnerItems);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SearchScreenActivity.this,
                android.R.layout.simple_spinner_item, spinnerItems);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rangeSpinner.setAdapter(adapter);
        rangeSpinner.setOnItemSelectedListener(this);

        //Setup locationmanager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //Setup
        searchScreen = new SearchScreen(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.searchBtn:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                userLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                MyLocation myUserLocation = new MyLocation(userLocation.getLatitude(), userLocation.getLongitude());
                if (userLocation.getTime() < Calendar.getInstance().getTimeInMillis() - TIME_DIFF) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                    userLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                ArrayList<Badestelle> resultList = searchScreen.doSearch(myUserLocation, searchRadius);

                if (resultList != null && resultList.size() > 0) {
                    Intent mapIntent = new Intent(this, MapsScreenActivity.class);
                    mapIntent.putExtra("badestellen", new DataWrapper(resultList));
                    mapIntent.putExtra("userLocation", userLocation);
                    startActivity(mapIntent);
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (position) {
            case 0:
                searchRadius = DISTANCES[0];
                break;
            case 1:
                searchRadius = DISTANCES[1];
                break;
            case 2:
                searchRadius = DISTANCES[2];
                break;
            case 3:
                searchRadius = DISTANCES[3];
                break;
            case 4:
                searchRadius = DISTANCES[4];
                break;
            case 5:
                searchRadius = DISTANCES[5];
                break;
            default:
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
