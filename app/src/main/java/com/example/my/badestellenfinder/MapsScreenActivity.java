package com.example.my.badestellenfinder;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.example.my.helper.Badestelle;
import com.example.my.helper.DataWrapper;
import com.example.my.fragments.DetailFragment;

/**
 *  HTW-Berlin Semesterbeleg SS2016 Technik mobiler Systeme
 *
 *  @Author Christian Loell
 */

public class MapsScreenActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    private ArrayList<Badestelle> badestellenList;
    private ArrayList<Badestelle> filteredList;
    private HashMap<Marker, Badestelle> markerToBadestelle = new HashMap<>();
    private Location userLocation = null;

    private final int ZOOM_LEVEL = 15;
    private final int BEARING = 0;
    private final int TILT = 0;
    private final int MARGIN = 75;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_screen);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        //Setup filter button
        Button filterBtn = (Button) findViewById(R.id.filterBtn);
        filterBtn.setOnClickListener(this);

        //getting data from intent
        DataWrapper dw = (DataWrapper) getIntent().getSerializableExtra("badestellen");
        badestellenList = dw.getBadestellen();
        userLocation = (Location) getIntent().getParcelableExtra("userLocation");
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnInfoWindowClickListener(this);
        setupBadestellenMarkers(badestellenList);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        setupCameraForAllMarkers();
    }

    private void setMapViewToUserLocation() {
        if (userLocation != null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(userLocation.getLatitude(), userLocation.getLongitude()), ZOOM_LEVEL));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(userLocation.getLatitude(), userLocation.getLongitude()))      // Sets the center of the map to location user
                    .zoom(ZOOM_LEVEL)                   // Sets the zoom
                    .bearing(BEARING)                // Sets the orientation of the camera to east
                    .tilt(TILT)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }

    private void setupBadestellenMarkers(ArrayList<Badestelle> badestellenListForMarkers) {
        markerToBadestelle.clear();
        Marker userMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(userLocation.getLatitude(), userLocation.getLongitude())).title(this.getResources().getString(R.string.userMarker)));
        markerToBadestelle.put(userMarker, null);
        mMap.clear();
        if (badestellenListForMarkers != null) {
            for (Badestelle badestelle : badestellenListForMarkers) {
                LatLng badestellenPos = new LatLng(badestelle.getLat(), badestelle.getLon());
                Marker newMarker = mMap.addMarker(new MarkerOptions().position(badestellenPos).title(badestelle.getName()));
                markerToBadestelle.put(newMarker, badestelle);
            }
        }
    }

    private void setupCameraForAllMarkers() {
        if (markerToBadestelle.size() <= 1) {
            setMapViewToUserLocation();
            Toast.makeText(this, getResources().getString(R.string.noBadestellenMarkers), Toast.LENGTH_SHORT).show();
        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(getBoundingBoxForAllMarkers(), MARGIN));
            Toast.makeText(this, markerToBadestelle.size() + " " + getResources().getString(R.string.numberOfBadestellen), Toast.LENGTH_SHORT).show();
        }

    }

    private LatLngBounds getBoundingBoxForAllMarkers() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (Map.Entry<Marker, Badestelle> filterEntry : markerToBadestelle.entrySet()) {
            builder.include(filterEntry.getKey().getPosition());
        }
        return builder.build();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putSerializable("badestelle", markerToBadestelle.get(marker));

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);
        fragmentTransaction.add(R.id.detail_container, detailFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filterBtn:
                Intent filterIntent = new Intent(this, FilterScreenActivity.class);
                filterIntent.putExtra("badestellen", new DataWrapper(badestellenList));
                startActivityForResult(filterIntent, 2);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && data != null) {
            DataWrapper dw = (DataWrapper) data.getSerializableExtra("filteredList");
            filteredList = dw.getBadestellen();
            setupBadestellenMarkers(filteredList);
            setupCameraForAllMarkers();
        }
    }
}
